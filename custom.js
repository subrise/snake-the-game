/*!
 * Subrise Snake
 *
 * A simple snake game made by Subrise Games
 *
 * Author: Sammy Hubner
 * Date: thursday March 22 2012
 */

(function (window, document, undefined) {
	'use strict';

	// Initialize variables and classes
	var version = '0.0.1',
		requestAnimFrame = (function () {
			return  window.requestAnimationFrame   ||
				window.webkitRequestAnimationFrame ||
				window.mozRequestAnimationFrame    ||
				window.oRequestAnimationFrame      ||
				window.msRequestAnimationFrame     ||
				function(/* function */ callback, /* DOMElement */ element){
					window.setTimeout(callback, 1000 / 60);
				};
		}()),
		// Standard Game Template
		Game = function () {
			var that = this,
				prevTime = new Date(),
				currTime,
				elapsedTime,
				state = 'INIT';

			// functions to extend
			this.update   = function (timeElapsed) {};
			this.gameOver = function () {};

			// Methods to control the game
			this.getState = function () { return state; };
			this.playGame = function () { state = 'PLAY'; };
			this.endGame  = function () {
				state = 'GAME_OVER'; 
				that.gameOver();
			};
			this.runGame = function (canvas) {
				requestAnimFrame(that.runGame, canvas);
				// Determine the miliseconds elapsed between frames
				currTime = new Date(),
				elapsedTime  = currTime - prevTime;
				prevTime = currTime;

				if (state === 'PLAY') {
					that.update(elapsedTime);
				}
			};
		},

		// Game specific variables
		tileWidth  = 5,
		tileHeight = 5,
		tilesHor   = 106, // 107 but we count from 0
		tilesVer   = 58, // actually 59, see tilesHor

		Tile = function (pX, pY, type) {
			var tile = document.createElement('div'),
				x, y, left, top,
				that = this;

			tile.className  = 'tile ' + type;

			// Public methods
			this.getX = function () {
				return x;
			};

			this.getY = function () {
				return y;
			}

			this.setX = function (a) {
				x = a;
				left = x * tileWidth;
				tile.style.left = left + 'px';
			}

			this.setY = function (a) {
				y = a;
				top = y * tileHeight;
				tile.style.top = top + 'px';
			}

			this.setPosition = function (a, b) {
				that.setX(a);
				that.setY(b);
			}

			this.setColor = function (red, green, blue) {
				tile.style.backgroundColor = 'rgb(' + red + ',' + green + ',' + blue + ')';
			}

			this.getElement = function () {
				return tile;
			}

			this.setPosition(pX, pY);
			canvas.game.appendChild(tile);
		},

		Snake = function () {
			var direction = 'LEFT',
				moveThreshold = 100,
				timer = 0,
				body = [],
				isGrowing = false;
			
			this.newGame = function () {
				var i, l = body.length;
				// empty old body 
				for (i = 0; i < l; i += 1) {
					canvas.game.removeChild(body[i].getElement());
					body[i] = undefined
				}
				body = [];

				// create initial body
				for (i = 0; i < 3; i += 1) {
					body.push(new Tile(Math.floor(tilesHor / 2) - i, Math.floor(tilesVer / 2), 'snake'));
				}

				direction = 'LEFT';
				moveThreshold = 100;
			};

			this.getBody = function () {
				return body;
			}

			this.isCollidingWithTile = function (t) {
				var i, 
					l = body.length;

				for (i = 0; i < l; i += 1) {
					if (body[i].getX() === t.getX() && body[i].getY() === t.getY()) {
						return true;
					}
				}
				return false;
			}

			this.update = function (game, timeElapsed, gem) {
				var i, x, y, l, 
					red, green, blue,
					dRed, dGreen, dBlue;
				timer += timeElapsed;
				if (timer > moveThreshold) {
					// move body
					if (isGrowing) {
						isGrowing = false;
						body.push(new Tile(-1, -1, 'snake'));
					}

					red = 204;
					green = 0;
					blue = 153;

					dRed = Math.floor((102 - 204) / body.length);
					dGreen = Math.floor((51 - 0) / body.length);
					dBlue = Math.floor((153 - 153) / body.length);
					for (i = body.length - 1; i > 0; i -= 1) {
						body[i].setPosition(body[i-1].getX(), body[i-1].getY());
						body[i].setColor(red + (dRed * i), green + (dGreen *i), blue + (dBlue * i));
					}
					body[0].setColor(204, 0, 153);
					// move head
					if (direction === 'LEFT') {
						x = body[0].getX() + 1;
						if (x > tilesHor) {
							x = 0;
						}
						body[0].setX(x);
					} else if (direction === 'RIGHT') {
						x = body[0].getX() - 1;
						if (x < 0) {
							x = tilesHor;
						}
						body[0].setX(x);
					} else if (direction === 'UP') {
						y = body[0].getY() - 1;
						if (y < 0) {
							y = tilesVer;
						}
						body[0].setY(y);
					} else if (direction === 'DOWN') {
						y = body[0].getY() + 1;
						if (y > tilesVer) {
							y = 0;
						}
						body[0].setY(y);
					}
					// Check for collisions
					l = body.length;
					for (i = 1; i < l; i += 1) {
						// if collide with own body the game ends
						if (body[0].getX() === body[i].getX() && body[0].getY() === body[i].getY()) {
							game.endGame();
						}
						// if collide with a gem, the body gets longer
						if (body[0].getX() === gem.getX() && body[0].getY() === gem.getY()) {
							isGrowing = true;
							gem.reposition();
							game.addScore();
							moveThreshold -= 1;
						}
					}
					timer = 0;
				}
			};

			this.setDirection = function (d) {
				if (d !== 'LEFT' && d !== 'RIGHT' && d !== 'UP' && d !== 'DOWN') {
					return false;
				} else {
					if ((direction === 'LEFT' && d === 'RIGHT') ||
						(direction === 'RIGHT' && d === 'LEFT') ||
						(direction === 'UP' && d === 'DOWN')    ||
						(direction === 'DOWN' && d === 'UP')) {
						return false;
					} else {
						direction = d;
					}
				}
			};
		},

		Gem = function (threshold) {
			var x = -1, 
				y = -1, 
				timer = 0,
				that = this,
				tile = new Tile(x, y, 'gem');

			this.reposition = function () {
				timer = 0;
				// get random position
				x = Math.floor(Math.random() * (tilesHor + 1));
				y = Math.floor(Math.random() * (tilesVer * 1));
				tile.setPosition(x, y);
				// check if new position doesn't already collide with snake
				if (snake.isCollidingWithTile(tile)) {
					that.reposition();
				}
			};

			this.getX = tile.getX;
			this.getY = tile.getY;

			this.update = function (e) {
				timer += e;
				if (timer > threshold) {
					timer = 0;
					that.reposition();
				}
			};
		},

		canvas = {},
		score = 0,
		snakeTheGame = new Game(),
		snake = new Snake(),
		gem;



	snakeTheGame.init = function () {
		// Initialize dom elements
		canvas.game              = document.createElement('div');
		canvas.game.id           = 'gameCanvas';
		canvas.score             = document.createElement('div');
		canvas.score.id          = 'score';
		canvas.message           = document.createElement('div');
		canvas.message.id        = 'message';
		canvas.message.className = 'init';
		canvas.message.innerHTML = '<h1>Snake the Game</h1><p>Click to start the game, use the arrow keys to move</p>';

		// Append to screen
		canvas.game.appendChild(canvas.score);
		canvas.game.appendChild(canvas.message);
		document.body.appendChild(canvas.game);
		gem = new Gem();
		gem.reposition();
	}

	// reset game variables for a new game
	snakeTheGame.newGame = function () {
		score = 0;
		snake.newGame();
		canvas.score.innerHTML = 'Score: ' + score;
		canvas.message.className = 'close';
		this.playGame();
	};

	snakeTheGame.update = function (e) {
		// update code	
		snake.update(this, e, gem);
	};

	snakeTheGame.addScore = function () {
		score += 1;
		canvas.score.innerHTML = 'Score: ' + score;
	}

	snakeTheGame.gameOver = function () {
		canvas.message.innerHTML = '<h1>Game Over</h1><p>Click to play again.</p>';
		canvas.message.className = 'game_over';
	}



	// Play the game
	window.onload = function () {
		snakeTheGame.init();
		document.addEventListener('keydown', function (e) {
			if (e.keyCode === 39) {
				snake.setDirection('LEFT');
			} else if (e.keyCode === 37) {
				snake.setDirection('RIGHT');
			} else if (e.keyCode === 38 ) {
				snake.setDirection('UP');
			} else if (e.keyCode === 40) {
				snake.setDirection('DOWN');
			}
		}, false);
		canvas.game.addEventListener('mousedown', function (e) {
			if (snakeTheGame.getState() === 'GAME_OVER' || snakeTheGame.getState() === 'INIT') {
				snakeTheGame.newGame();
			}
		});
		snakeTheGame.runGame(canvas.game);
	}

}(this, this.document));